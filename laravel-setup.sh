#!/bin/bash

# functions
install_composer() {
    php -r "copy('https://getcomposer.org/installer', 'composer-setup.php');";
    php -r "if (hash_file('SHA384', 'composer-setup.php') === '544e09ee996cdf60ece3804abc52599c22b1f40f4323403c44d44fdfdd586475ca9813a858088ffbc1f233e9b180f061') { echo 'Installer verified'; } else { echo 'Installer corrupt'; unlink('composer-setup.php'); } echo PHP_EOL;";
    php composer-setup.php;
    php -r "unlink('composer-setup.php');";
    sudo mv composer.phar /usr/local/bin/composer;
}

homebrew_install() {
  /usr/bin/ruby -e "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/master/install)"
}

npm_install() {
    if type "npm" &> /dev/null
    then
        echo
        echo "Installing npm packages..."
        echo
        npm install
    else
	read -r -p "Node and npm aren’t installed. Do you want to install it? (Y/N) " response
	if [[ "$response" =~ ^([yY][eE][sS]|[yY])+$ ]]
		then
		    if !(type "brew" &> /dev/null)
		        then
    		    homebrew_install
    		fi
	        brew upgrade
		    brew install nodejs
	else
		exit 0;
	fi
    fi
}

set_env(){
    f=.env
    read -r -p "Do you want to set up database config? (Y/N) " response
	if [[ "$response" =~ ^([yY][eE][sS]|[yY])+$ ]]
		then
    		read -p 'DB_DATABASE: ' db_database
            read -p 'DB_USERNAME: ' db_username
            read -p 'DB_PASSWORD: ' db_password
            sed -i -e "s/DB_HOST=127.0.0.1/DB_HOST=localhost/g" "$f"
            sed -i -e "s/DB_DATABASE=homestead/DB_DATABASE=$db_database/g" "$f"
            sed -i -e "s/DB_USERNAME=homestead/DB_USERNAME=$db_username/g" "$f"
            sed -i -e "s/DB_PASSWORD=secret/DB_PASSWORD=$db_password/g" "$f"
            echo
            echo "File .env updated successfully!"
            echo
	fi
}


# Laravel setup
if [ -z "$1" ]
    then
        echo "ERROR: Insert a folder application name!";
else if type "laravel" &> /dev/null
    then
	    # exec laravel new command
        laravel new "$1";
        cd "$1";
        set_env
        npm_install
else if type "composer" &> /dev/null
    then
	    composer create-project --prefer-dist laravel/laravel "$1"
	    cd "$1";
	    cp .env.example .env
	    php artisan key:generate
	    set_env
	    npm_install
else
    install_composer
    sh laravel-setup.sh "$1"
fi
fi
fi